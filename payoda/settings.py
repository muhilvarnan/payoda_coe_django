"""
Django settings for payoda project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '4d_sbb0s80%#60_@ce5^l3!(%oqghdo6ytkh+7zla$=3tp&(_d'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
	'coe',
	'getpass',
    'dajaxice',
    'PIL',
    'requests'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'payoda.urls'

WSGI_APPLICATION = 'payoda.wsgi.application'

PROJECT_DIR = os.path.dirname(__file__)

TEMPLATE_DIRS = ( 
    os.path.join(PROJECT_DIR, '../templates'),
)

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',
    'django.template.loaders.eggs.Loader',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
    'django.core.context_processors.static',
    'django.core.context_processors.request',
    'django.contrib.messages.context_processors.messages'
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    'dajaxice.finders.DajaxiceFinder',
)

AUTHENTICATION_BACKENDS = (
 'django_ldapbackend.LDAPBackend',
 'django.contrib.auth.backends.ModelBackend',
)


AUTH_LDAP_SERVER_URI = "ldap://10.10.100.4"                       # Hostname
AUTH_LDAP_BIND_DN = "CN=ldapbind user,OU=Java Team,OU=Payoda Users,DC=payoda,DC=com"   # Administrative User's Username
AUTH_LDAP_BIND_PASSWORD = "payoda@123"                     # Administrative User's Password 
AUTH_LDAP_USER_SEARCH = "OU=Payoda Users,DC=payoda,DC=com"
AUTH_LDAP_BASE_DN = "OU=Payoda Users,DC=payoda,DC=com"              # Base DN (also accepts o=example.com format)
AUTH_LDAP_FIELD_DOMAIN = "payoda.com"               # Domain from which users will take the domain for dummy e-mail generation (it keeps Django happy!)
AUTH_LDAP_GROUP_NAME = "ldap_people"                 # Django group for LDAP users (helps us manage them for password changing, etc.)
AUTH_LDAP_VERSION = 3                                # LDAP version
AUTH_LDAP_OLDPW = False                              # Can the server take the old password? True/False

# Optional
AUTH_LDAP_FIELD_USERAUTH = "uid"                     # The field from which the user authentication shall be done.
AUTH_LDAP_FIELD_AUTHUNIT = "People"                  # The organisational unit in which your users shall be found.
AUTH_LDAP_FIELD_USERNAME = "uid"                     # The field from which to draw the username (Default 'uid'). (Allows non-uid/non-dn custom fields to be used for login.)
AUTH_LDAP_WITHDRAW_EMAIL = False                     # Should django try the directory for the user's email ('mail')? True/False.

AUTH_LDAP_USER_ATTR_MAP = {
    "first_name": "sAMAccountType",
    "last_name": "sn",
    "email": "mail"
}

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', 
        'NAME': 'payoda',                     
        'USER': 'root',
        'PASSWORD': '',
		'PORT':'3306',
		'HOST':'127.0.0.1'
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/


STATIC_URL = '/static/'



