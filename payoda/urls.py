from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()
from dajaxice.core import dajaxice_autodiscover, dajaxice_config
dajaxice_autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'payoda.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    #url(r'^coe/', include('blog.urls')),
    url(r'^search/', 'coe.views.search'),
    url(r'^login/', 'coe.views.login'),
    url(r'^logout/', 'coe.views.logout'),
    #url(r'^search/', 'coe.views.search'),
	url(r'^$', 'coe.views.index'),
    url(r'^index/', 'coe.views.index'),
	#ajax
	url(dajaxice_config.dajaxice_url, include('dajaxice.urls')),
)
