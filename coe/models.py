from django.db import models
from ldap_custom_functions import ldap_custom_search,ldap_custom_initialize,get_different_days

class Bravo(models.Model):
    title= models.CharField(max_length=100) 
    description = models.TextField(max_length=300)
    created_date=models.DateTimeField(auto_now=True)
    bravo_pic = models.ImageField(upload_to = 'coe/static/bravo/', default = 'coe/static/bravo/default.jpg')
    def __unicode__(self):  # Python 3: def __str__(self):
        return self.title


class Bravo_action(models.Model):
    awarded_by = models.CharField(max_length=100)
    awarded_to = models.CharField(max_length=100)
    awarded_for= models.TextField(max_length=1000)
    bravo_id = models.CharField(max_length=100)
    timestamp = models.DateTimeField(auto_now=True)

def  _getuserpopdetails(name,username):  
    ldap_obj = ldap_custom_initialize()
    searchFilter = "sAMAccountName="+name+""
    searchData = ldap_custom_search(ldap_obj, searchFilter)
    content = searchData[0][0][1]   
    days = get_different_days(content['whenCreated'][0])
    bravoItems = Bravo.objects.all().values_list('id','title','description','bravo_pic')
    bravoAction = Bravo_action.objects.filter(awarded_to=name).values_list('awarded_by','awarded_to','awarded_for','bravo_id')
    bravoAwards = dict()
    manager = content['manager'][0]
    start = manager.index( 'CN=' ) + len( 'CN=' )
    end = manager.index( ',', start )
    manager = manager[start:end]
    i = 0
    for action in bravoAction:
        bravoId = action[3]
        bravodetail = Bravo.objects.filter(id=bravoId).values_list('id','title','description','bravo_pic')
        d={'a':[],'b':[]}
        d['a'].append(action)
        d['a'].append(bravodetail)
        bravoAwards[i] = d['a']
        i= i+1
    if username is None:   
        context = {
                'awardgivento': name,
                'loginStaus':0,
                'content':content,
                'days':days,
                'bravoItems':bravoItems,
                'bravoawards':bravoAwards,
                'manager':manager
             }
    else:
        context = {
                'awardgivenby': username,
                'awardgivento': name,
                'loginStaus':1,
                'content':content,
                'days':days,
                'bravoItems':bravoItems,
                'bravoawards':bravoAwards,
                'manager':manager
             }   
    return context
