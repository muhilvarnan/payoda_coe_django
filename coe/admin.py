from django.contrib import admin

from coe.models import Bravo_action,Bravo

class MemberDetailAdmin(admin.ModelAdmin):
    list_display = ("id","awarded_by", "awarded_to", "awarded_for", "bravo_id", "timestamp")

admin.site.register(Bravo_action, MemberDetailAdmin)
admin.site.register(Bravo)