from django.http import StreamingHttpResponse
from django.template import RequestContext, loader
from coe.models import _getuserpopdetails,Bravo_action
from dajaxice.decorators import dajaxice_register
import requests,json

def index(request):
    template = loader.get_template('index.html')
    if 'coe_has_logged' in request.session:
        username = request.session['coe_username']
        context = RequestContext(request, {
            'username':username,
            'loginStaus':1
         })
    else:
        context = RequestContext(request, {
            'loginStaus':0
        })
    return StreamingHttpResponse(template.render(context))

def search(request): 
    from ldap_custom_functions import ldap_custom_initialize,ldap_custom_search
    ldap_obj = ldap_custom_initialize()
    q = request.POST.get('q')
    template = loader.get_template('search.html')
    if q is None: 
        searchData = None
        searchCount= 0
        q=''
        if 'coe_has_logged' in request.session:
            username = request.session['coe_username']
            context = RequestContext(request, {
                'username':username,
                'loginStaus':1,
                'searchData':searchData,
                'searchCount': searchCount,
                'q':q 
            })
        else:
            context = RequestContext(request, {
                'loginStaus':0,
                'searchData':searchData,
                'searchCount': searchCount,
                'q':q 
            })
    else: 
        searchFilter = "cn=*"+q+"*"
        searchData = ldap_custom_search(ldap_obj, searchFilter)
        wikiData=get_wiki_data(q);
        searchCount = len(searchData)
        if 'coe_has_logged' in request.session:
            username = request.session['coe_username']
            context = RequestContext(request, {
                'username':username,
                'loginStaus':1,
                'searchData':searchData,
                'wikiData': wikiData,
                'searchCount': searchCount,
                'q':q 
            })
        else:
            context = RequestContext(request, {
                'loginStaus':0,
                'searchData':searchData,
                'wikiData': wikiData,
                'searchCount': searchCount,
                'q':q 
            })
    return StreamingHttpResponse(template.render(context))

def login(request):
    if 'coe_has_logged' in request.session:
        username = request.session['coe_username']
        template = loader.get_template('index.html')
        context = RequestContext(request, {
            'username':username,
            'loginStaus':1
         })
    from ldap_custom_functions import ldap_custom_login
    username = request.POST.get('username')
    password = request.POST.get('password')
    loginStatus = ldap_custom_login(username,password)
    if loginStatus==1:
        request.session['coe_username'] = username
        request.session['coe_has_logged'] = True
        template = loader.get_template('index.html')
        context = RequestContext(request, {
            'username':username,
            'loginStaus':1
        })
    else:
        template = loader.get_template('index.html')
        context = RequestContext(request, {
            'username':username,
            'loginStaus':loginStatus
        })
    return StreamingHttpResponse(template.render(context))

def logout(request):
    del request.session['coe_has_logged']
    del request.session['coe_username']
    template = loader.get_template('index.html')
    context = RequestContext(request,{
                                      'loginStaus':0
                                      })
    return StreamingHttpResponse(template.render(context))

def get_wiki_data(key):
    payload = {'prop':'text','action': 'query', 'list': 'search','srsearch':key,'format':'json'}
    r=requests.get('http://182.72.85.87/wiki/api.php',params=payload)
    data=json.loads(r.text)
    results=data['query']['search']
    for result in results:
        url=result['title'].replace (" ", "_")
        result['url']='http://182.72.85.87/wiki/index.php/'+url
    return results

@dajaxice_register
def getUserDetail(request, name):
    if 'coe_has_logged' in request.session: 
        username = request.session['coe_username']
        contextParams = _getuserpopdetails(name,username)
    else:
        contextParams = _getuserpopdetails(name,None)   
    template = loader.get_template('user_popup.html')
    context = RequestContext(request,contextParams) 
    return StreamingHttpResponse(template.render(context))  

@dajaxice_register
def giveBravoAction(request,bravoId,yBravo,awardBy,awardFor,bravotitle):
    name = awardBy
    Bravo_action(awarded_by=awardBy,awarded_to=awardFor,bravo_id=bravoId,awarded_for=yBravo).save()
    if 'coe_has_logged' in request.session: 
        username = request.session['coe_username']
        contextParams = _getuserpopdetails(name,username)
    else:
        contextParams = _getuserpopdetails(name,None)   
    template = loader.get_template('user_popup.html')
    context = RequestContext(request,contextParams) 
    return StreamingHttpResponse(template.render(context))  

