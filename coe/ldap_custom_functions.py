# logic.py
from django.conf import settings
import ldap

class DoesNotCompute(Exception):
    pass

def ldap_custom_initialize():
	try:
		server = settings.AUTH_LDAP_SERVER_URI 	
		user_dn = settings.AUTH_LDAP_BIND_DN
		user_pw = settings.AUTH_LDAP_BIND_PASSWORD
		ldab_obj = ldap.initialize(server)
		ldab_obj.protocol_version = ldap.VERSION3
		ldab_obj.simple_bind_s(user_dn, user_pw)
		return ldab_obj
	except TypeError:
		raise DoesNotCompute

def ldap_custom_search(ldab_obj, searchFilter):
	try:
		ldab_obj.protocol_version = ldap.VERSION3
		baseDN = settings.AUTH_LDAP_USER_SEARCH
		searchScope = ldap.SCOPE_SUBTREE
		retrieveAttributes = None
		ldap_result_id = ldab_obj.search(baseDN, searchScope, searchFilter, retrieveAttributes)
		result_set = []
		while 1:
			result_type, result_data = ldab_obj.result(ldap_result_id, 0)
			if (result_data == []):
				break
			else:
				if result_type == ldap.RES_SEARCH_ENTRY:
					result_set.append(result_data)
		return result_set		
	except TypeError:
		raise DoesNotCompute

def ldap_custom_login(username, password):
	try:
		ldap_obj = ldap_custom_initialize()		
		searchFilter = "sAMAccountName="+ username
		loginStatus = ldap_custom_search(ldap_obj, searchFilter)  
		if not loginStatus:   
			return 0
		user_dn=loginStatus[0][0][0]       
		user_pw = password
		ldap_obj.simple_bind_s(user_dn, user_pw)
		return 1
	except ldap.LDAPError:
			return 0

def get_different_days(created):
	try:				
		created_year = created[0:4]
		created_month = created[4:6]
		created_date = created[6:8]
		fromdate = ""+created_year+"-"+created_month+"-"+created_date
		#result = "%d years, %d months, %d days, %d hours" % (rd.years, rd.months, rd.days, rd.hours) # 3 years, 6 months, 9 days, 1 hours, 11 minutes and 41 seconds	
		return fromdate
	except TypeError:
		raise DoesNotCompute		


#Assign DoesNotCompute exception to this_function
ldap_custom_initialize.DoesNotCompute = DoesNotCompute
ldap_custom_search.DoesNotCompute = DoesNotCompute
get_different_days.DoesNotCompute = DoesNotCompute