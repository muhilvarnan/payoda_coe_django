from django.conf.urls import url

from coe import views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    # ex: /polls/
    #url(r'^$', views.index, name='index'),
    # ex: /polls/5/
    #url(r'^search/(?P<q>[0-9]+)/$', views.search, name='search'),
    urlpatterns += staticfiles_urlpatterns()
]